@extends('admin::layouts.master')
@section('title', 'Priests ID')

@section('css')
@stop

@section('content')

<div class="content-wrapper">
@if(Session::has('val'))
            @if(Session::get('val')==1)
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="padding-right: 14px;">×</button>
                    <h4><i class="icon fa fa-check"></i> Success!&nbsp;&nbsp;</h4>
                    <p>{!! Session::get('msg') !!}</p>
                </div>
            @endif
            @if(Session::get('val')==0)
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!&nbsp;&nbsp;</i></h4>
                        <p>{!! Session::get('msg') !!}</p>
                </div>
            @endif
@endif
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Dioces Priests ID List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">All Priests</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                <a class="btn btn-primary btn-sm" href="{{URL('/admin/priests/id/download')}}" onclick="exportTasks(event.target);">
                                    <i class="fas fa-download">
                                    </i>
                                    Download for ID card processing
                                </a>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Home Parish</th>
                                            <th>Address</th>
                                            <th>Mobile</th>
                                            {{--<th>Photo</th>--}}
                                            <th>ID Status</th>
                                            <th>Date of issue</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($priests_id as $priest)
                                        <tr>
                                            <td>{{$priest->baptism_name}}</td>
                                            <td>{{$priest->home_parish}}</td>
                                            <td>{{$priest->address}}</td>
                                            <td>{{$priest->mobile}}</td>
                                            <td>@if($priest->status == 0) <span class="badge badge-warning">Pending</span> @else <span class="badge badge-success">Completed</span> @endif</td>
                                            <td>{{$priest->date_issue}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Home Parish</th>
                                        <th>Address</th>
                                      {{--  <th>Photo</th>--}}
                                        <th>Mobile</th>
                                        <th>ID Status</th>
                                        <th>Date of issue</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- /.content-wrapper -->

@stop

@section('js')
<script>
   function exportTasks(_this) {
      let _url = $(_this).data('href');
      window.location.href = _url;
   }
</script>
@stop