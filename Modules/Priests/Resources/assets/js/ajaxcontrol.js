$(function() 
{
    $('#checked_id').click(function(){
        var favorite = [];
            $.each($("input[name='id_card']:checked"), function(){
                favorite.push($(this).val());
            });
            $.ajax({
                headers: 
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: base_url+"/admin/priests/id/upload",
                type: "POST",
                data: {id:favorite.join(", ")},
                success: function(response)
                {
                     if(response.status==true){window.location.href = response.url; }
                     else{$(".showinfo").html(response.alert);}
                }
            });
    })

});
