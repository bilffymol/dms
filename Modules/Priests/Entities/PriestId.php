<?php

namespace Modules\Priests\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PriestId extends Model
{
    protected $table = "admin_priestid";
    protected $fillable = ['id', 'official_name', 'email', 'mobile'];
}
