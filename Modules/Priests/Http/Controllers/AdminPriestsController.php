<?php

namespace Modules\Priests\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Modules\Priests\Entities\PriestId;
use Carbon\Carbon;

class AdminPriestsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $priests = DB::connection('diocese')->table('diocese_priest')->get();
        return view('priests::admin.index',compact('priests'));
    }

    public function allPriestId()
    {
        $priests_id = PriestId::get();
        return view('priests::admin.idindex',compact('priests_id'));
    }

    public function upload(Request $request)
    {
        $priestid = explode(", ",$request->id);
        foreach ($priestid as $key => $value) {
            $priest_details = DB::connection('diocese')->table('diocese_priest')->where('id',$value)->first();
            $priestid_data = new PriestId;
            $priestid_data->priest_diocese_id = $priest_details->id;
            $priestid_data->official_name =  $priest_details->CALL_NAME;
            $priestid_data->baptism_name = 'Fr. '.$priest_details->ADRS1.' '.$priest_details->NAME;
            $priestid_data->priest_type = 'Catholic Priest';
            $priestid_data->priest_id = substr($priest_details->DOO, 2, 2).'/'.substr($priest_details->CODE, 0, 3).'/'.substr($priest_details->CODE, 3, 3);
            $priestid_data->ordined_date = $priest_details->DOO;
            $priestid_data->home_parish = $priest_details->PARISH;
            $priestid_data->email = $priest_details->EMAIL;
            $priestid_data->mobile = $priest_details->MOBILE;
            $priestid_data->address = $priest_details->ADRS2;
            $priestid_data->nationality = 'Indian';
            $priestid_data->status = '0';
            try{
                $priestid_data->save();
            }catch (\Exception $e){
                $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
                return response()->json(['status'=>FALSE,'alert'=>$html,'csrf' => csrf_token()]);
            }
        }
        $request->session()->flash('val', 1);
        $request->session()->flash('msg', "Successfully uploaded the details of priests for ID processing !");
        return response()->json(['status'=>true,'url'=>URL('/admin/priests/'),'csrf' => csrf_token()]);
    }

    public function exportCsv(Request $request)
    {
        $fileName = 'priests_id.csv';
        $priests_id = PriestId::all();
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );
        $columns = array('Official name', 'Baptism name', 'Priest type', 'Priest id', 'Ordined date', 'Home parish', 'Email', 'Mobile Number', 'Address', 'Nationality');
        // $callback = function() use($priests_id, $columns) {
            $file = fopen($fileName, 'w+');
            fputcsv($file, $columns);
            foreach ($priests_id as $priest) {
                if($priest->status == 0){
                    $row['Official name']  = $priest->official_name;
                    $row['Baptism name']    = $priest->baptism_name;
                    $row['Priest type']    = $priest->priest_type;
                    $row['Priest id']  = $priest->priest_id;
                    $row['Ordined date']  = $priest->ordined_date;
                    $row['Home parish']    = $priest->home_parish;
                    $row['Email']    = $priest->email;
                    $row['Mobile Number']  = $priest->mobile;
                    $row['Address']    = $priest->address;
                    $row['Nationality']  = $priest->nationality;
                    $priest_update= PriestId::where('id',$priest->id)->first();
                    $priest_update->status = 1;
                    $priest_update->date_issue = Carbon::now()->toDateTimeString();
                    $priest_update->save();
                    fputcsv($file, array($row['Official name'], $row['Baptism name'], $row['Priest type'], $row['Priest id'], $row['Ordined date'],$row['Home parish'], $row['Email'], $row['Mobile Number'], $row['Address'], $row['Nationality']));
                }
            }
            fclose($file);
        // };
        return response()->download($fileName, 'Priest ID List '.date("d-m-Y H:i").'.csv', $headers);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('priests::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('priests::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('priests::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
