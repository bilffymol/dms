<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriestIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_priestid', function (Blueprint $table) {
            $table->bigIncrements('id');  
            $table->bigInteger('priest_diocese_id')->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('official_name',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('baptism_name',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('priest_type',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('priest_id',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('ordined_date',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('home_parish',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('email',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('mobile',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('address',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('nationality',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('blood_group',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->tinyInteger('status')->comment('0-pending , 1-completed')->nullable();
            $table->dateTime('date_issue')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_priestid');
    }
}
