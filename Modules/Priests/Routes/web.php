<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web'], 'prefix' => 'admin/priests'], function()
{

      /* logged users opertaions */
    Route::group(['middleware' =>  'admin_auth:admin'], function()
    {
        Route::get('/', ['middleware' => 'check_perm:priest-list', 'uses' => 'AdminPriestsController@index']);
        Route::post('/id/upload', ['middleware' => 'check_perm:priest-list', 'uses' => 'AdminPriestsController@upload']);
        Route::get('/id', ['middleware' => 'check_perm:priest-id-list', 'uses' => 'AdminPriestsController@allPriestId']);
        Route::get('/id/download', ['middleware' => 'check_perm:priest-id-list', 'uses' => 'AdminPriestsController@exportCsv']);
        // Route::get('/AdminPriestsList',['middleware' => 'check_perm:priest-list','uses' => 'AdminPriestsController@allAdtypes']);
        Route::get('/add', ['middleware' => 'check_perm:priest-create', 'uses' => 'AdminPriestsController@create']);
        Route::post('/create', ['middleware' => 'check_perm:priest-create', 'uses' => 'AdminPriestsController@store']);
        Route::post('changeStatus', ['middleware' => 'check_perm:priest-edit', 'uses' => 'AdminPriestsController@changeStatus']);
        Route::get('delete/{id}', ['middleware' => 'check_perm:priest-delete', 'uses' => 'AdminPriestsController@destroy']);
        Route::get('/edit/{id}', ['middleware' => 'check_perm:priest-edit', 'uses' => 'AdminPriestsController@edit']);
        Route::get('/view/{id}', ['middleware' => 'check_perm:priest-list', 'uses' => 'AdminPriestsController@show']);
        Route::post('/update/{id}', ['middleware' => 'check_perm:priest-edit', 'uses' => 'AdminPriestsController@update']);
     });
});