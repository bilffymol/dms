$(function() 
{
    $("#reset_page").hide();
    
    $("#login_signin_submit").click(function(e) 
    {
        // validate and process form here
        e.preventDefault(); 
        $.ajax({
            headers: 
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url:base_url+"/admin/post_login",
            dataType: "json",
            async: false, 
            data: new FormData($('#LoginForm')[0]),
            processData: false,
            contentType: false, 
            success: function(response)
            {
                if(response.status==true)
                {
                    $("#Email").removeClass("validate is-invalid");
                    $("#Email .invalid-feedback").html(' ');

                    $("#Password").removeClass("validate is-invalid");
                    $("#Password .invalid-feedback").html(' ');
                    
                    window.location.href = response.url;
                
                }
                else
                {
                  $("#login_error").show().html(response.alert);  
                }
            },
            error: function (request, textStatus, errorThrown) {
                if(request.responseJSON.message == 'Too Many Attempts.'){
                  $("#login_error").html('<div class="alert alert-danger" role="alert"><strong>Alert!&nbsp;</strong>Too many attempts. Please try after 2 minutes.</div>');
                }
                if(request.responseJSON.errors.password)
                {
                    $("#Password").addClass("validate is-invalid");
                    $("input[name='password']").addClass("is-invalid");
                    $("#Passwordnew.invalid-feedback").html(request.responseJSON.errors.password[0]);
                    $("#Passwordnew.invalid-feedback").show();
                }
                if(request.responseJSON.errors.email.length)
                {
                    $("#Email").addClass("validate is-invalid");
                    $("input[name='email']").addClass("is-invalid");
                    $("#Email .invalid-feedback").html(request.responseJSON.errors.email[0]);
                    $("#Email .invalid-feedback").show();
                } 
            }
        });
                        
    });

      
            $(".kt-login__link-forgot").click(function(e){
            e.preventDefault(); 
            $("#login_page").hide();
            $("#reset_page").show();
            });



    $('#login_forgot_submit').click(function(e){
        e.preventDefault(); 
         $.ajax({
            headers: 
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url:base_url+"/admin/forgot_password",
            dataType: "json",
            async: false, 
            data: new FormData($('#ForgotPasForm')[0]),
            processData: false,
            contentType: false, 
            success: function(response)
            {
                if(response.status == true){
                    $("#email_reset").removeClass("validate is-invalid");
                    $("#email_reset .invalid-feedback").html(' ');
                    window.location.href = response.url;
                }else{
                    $("#resetemail_error").show().html(response.alert); 
                }
            },
            error: function (request, textStatus, errorThrown) {

                if(request.responseJSON.errors.email_reset.length)
                {
                    if(request.responseJSON.errors.email_reset[0] == "The email reset field is required."){
                    request.responseJSON.errors.email_reset[0] = "This field is required.";}
                    $("#email_reset").addClass("has-error");
                    $("input[name='email_reset']").addClass("is-invalid");
                    $(".invalid-feedback").html(request.responseJSON.errors.email_reset[0]);
                    $(".invalid-feedback").show();
                }
            }

         });
    });

    $('#reset_submit').click(function(e){
        e.preventDefault(); 
        var new_password     = $("#Password").val().trim();
        var confirm_password = $("#CPassword").val().trim();
        var action           =   $("#ResetForm").attr('action');
        var a = 0; var b = 0;

        if(new_password.length == 0){a=0;
            $("#Password").addClass("has-error");
            $("input[name='Password']").addClass("is-invalid");
            $("#PasswordCheck.invalid-feedback").show().html("This field is required.");
        }     
        else{a=1;
            $("#Password").removeClass("has-error");
            $("input[name='Password']").removeClass("is-invalid");
            $( "#PasswordCheck" ).html('');
        }

        if(confirm_password.length == 0){b=0;
            $("#CPassword").addClass("has-error");
            $("input[name='CPassword']").addClass("is-invalid");
            $("#CPasswordCheck.invalid-feedback").show().html("This field is required.");
        }else{b=1;
            $("#CPassword").removeClass("has-error");
            $("input[name='CPassword']").removeClass("is-invalid");
            $( "#CPasswordCheck" ).html('');
        }
        
        if(a==1 && b==1){
            
            if(new_password == confirm_password){
                $.ajax({
                    headers: 
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url:action,
                    dataType: "json",
                    async: false, 
                    data: new FormData($('#ResetForm')[0]),
                    processData: false,
                    contentType: false, 
                    success: function(response)
                    {
                        if(response.status == true){
                        $("#Password").removeClass("has-error");
                        $("#Password .help-block").html(' ');
                        $("#CPassword").removeClass("has-error");
                        $("#CPassword .help-block").html(' ');
                        $("#reset_success").show().html(response.html); 
                        setTimeout(function(){ window.location = response.url; }, 3000);
                        }else{
                            $("#reset_error").show().html(response.html); 
                        }
                    },
                    error: function (request, textStatus, errorThrown) {
                        
                    }
                });
                }else{
                $("#CPassword").addClass("has-error");
                $("input[name='CPassword']").addClass("is-invalid");
                $("#CPasswordCheck.invalid-feedback").show().html("Please confirm your password.");
                }    
            
        }
    
    });

});
