<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Pala Diocese | Recover Password</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL::asset('storage/demo/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ URL::asset('storage/demo/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset('storage/demo/dist/css/adminlte.min.css') }}">
  <link rel="shortcut icon" href="{{ URL::asset('storage/logo/logo.png') }}" />
</head>
<script>
    var base_url = "{{URL::to('/')}}";
</script>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>Pala Diocese</b>
  </div>
  <!-- /.login-logo -->
  <?php 
    $checkuser = session()->get('checkuser');
    if(is_null($checkuser)){
        $checkuser = $checkuser_send;
    }                        
  ?>
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">You are only one step a way from your new password, recover your password now.</p>
      <form action="{{URL('/admin/change_password').'/'.$checkuser->id}}" id="ResetForm" method="post">
      <input type="hidden" value="{{$checkuser->email}}" name="email">
        <div class="input-group mb-3">
        <input class="form-control" type="password" placeholder="Enter new password" name="Password" id="Password">       
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <div id="PasswordCheck" class="invalid-feedback"></div>
        </div>
        <div class="input-group mb-3">
        <input class="form-control" type="password" placeholder="Confirm new password" name="CPassword" id="CPassword">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <div id="CPasswordCheck" class="invalid-feedback"></div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" id="reset_submit" class="btn btn-primary btn-block">Change password</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mt-3 mb-1">
        <a href="{{ URL('/admin/login') }}">Login</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ URL::asset('storage/demo/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ URL::asset('storage/demo/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('storage/demo/dist/js/adminlte.min.js') }}"></script>
<script src="{{ Module::asset('admin:js/login.js') }}"></script>
</body>
</html>
