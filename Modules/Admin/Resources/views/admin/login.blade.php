<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Pala Diocese | Login</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL::asset('storage/demo/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ URL::asset('storage/demo/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset('storage/demo/dist/css/adminlte.min.css') }}">
  <link rel="shortcut icon" href="{{ URL::asset('storage/logo/logo.png') }}" />
</head>
<script>
    var base_url = "{{URL::to('/')}}";
</script>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>Pala Diocese</b>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body" id="login_page">
      <p class="login-box-msg">Sign In</p>
      <div id="login_error"></div>
      <form action="" id="LoginForm" method="post">
        <div class="input-group mb-3" id="Email">
          <input class="form-control" type="text" placeholder="Email" name="email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          <div class="invalid-feedback"></div>
        </div>
        <div class="input-group mb-3" id="Password">
        <input class="form-control" type="password" placeholder="Password" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <div id="Passwordnew" class="invalid-feedback"></div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember" name="remember_me">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" id="login_signin_submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mb-1">
        <a href="#" class="kt-link kt-login__link-forgot">I forgot my password</a>
      </p>
    </div>

    <div class="card-body login-card-body" id="reset_page">
      <p class="login-box-msg">Forgotten Password ?</p>
      <p class="login-box-msg">Enter your email to reset password:</p>
      <div id="resetemail_error"></div>
      <form action="" id="ForgotPasForm" method="post">
        <div class="input-group mb-3" id="Email">
        <input class="form-control" type="text" placeholder="Email" name="email_reset" id="email_reset" autocomplete="off">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          <div class="invalid-feedback"></div>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" id="login_forgot_submit" class="btn btn-primary btn-block">Request</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ URL::asset('storage/demo/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ URL::asset('storage/demo/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('storage/demo/dist/js/adminlte.min.js') }}"></script>
<script src="{{ Module::asset('admin:js/login.js') }}"></script>
</body>
</html>
