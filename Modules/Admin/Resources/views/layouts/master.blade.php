<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Pala Diocese | @yield('title')</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL::asset('storage/demo/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{ URL::asset('storage/demo/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ URL::asset('storage/demo/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset('storage/demo/dist/css/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ URL::asset('storage/demo/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ URL::asset('storage/demo/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ URL::asset('storage/demo/plugins/summernote/summernote-bs4.min.css') }}">
  <link rel="shortcut icon" href="{{ URL::asset('storage/logo/logo.png') }}" />
  @yield('css')
  <script>
    var base_url = "{{URL::to('/')}}";
  </script>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <!-- <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="{{ URL::asset('storage/logo/logo.png') }}" alt="Pala Diocese" height="60" width="60">
  </div> -->

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Settings</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Profile</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>

      <!-- Messages Dropdown Menu -->

      <!-- Notifications Dropdown Menu -->
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{ URL::asset('storage/logo/logo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Pala Diocese</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        @if(Auth::guard('admin')->user()->image)
          <img class="" alt="Pic" src="{{URL('storage/users/profile/').'/'.Auth::guard('admin')->user()->image}}" />
        @else
          <img src="{{ URL::asset('storage/demo/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        @endif
        </div>
        <div class="info">
          <a href="#" class="d-block">{{Auth::guard('admin')->user()->name}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ URL('/admin/dashboard') }}" class="nav-link {{{ (Request::is('admin/dashboard') ? 'active' : '') }}}">
              <i class="nav-icon fas fa-th"></i>
              <p>
              Dashboard
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
          </li>
          <li class="nav-header">User Management</li>
          <li class="nav-item {{{ (Request::is('admin/users') || Request::is('admin/users/add') || Request::is('admin/users/edit/*') || Request::is('admin/users/view/*') || Request::is('admin/roles') || Request::is('admin/roles/add') || Request::is('admin/roles/edit/*') || Request::is('admin/roles/view/*') ? 'menu-open' : '') }}}">
            <a href="#" class="nav-link {{{ (Request::is('admin/users') || Request::is('admin/users/add') || Request::is('admin/users/edit/*') || Request::is('admin/users/view/*') ? 'active' : '') }}}">
              <i class="nav-icon ion ion-person"></i>
              <p>
                Users
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ URL('/admin/roles') }}" class="nav-link {{{ (Request::is('admin/roles') || Request::is('admin/roles/add') || Request::is('admin/roles/edit/*') || Request::is('admin/roles/view/*') ? 'active' : '') }}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Roles</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ URL('/admin/users') }}" class="nav-link {{{ (Request::is('admin/users') || Request::is('admin/users/add') || Request::is('admin/users/edit/*') || Request::is('admin/users/view/*') ? 'active' : '') }}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>All Users</p>
                </a>
              </li>
              <!-- <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v3</p>
                </a>
              </li> -->
            </ul>
          </li>
          <li class="nav-header">Priest Management</li>
          <li class="nav-item {{{ (Request::is('admin/priests/id') || Request::is('admin/priests') || Request::is('admin/priests/add') || Request::is('admin/priests/edit/*') || Request::is('admin/priests/view/*') ? 'menu-open' : '') }}}">
            <a href="#" class="nav-link {{{ (Request::is('admin/priests/id') || Request::is('admin/priests') || Request::is('admin/priests/add') || Request::is('admin/priests/edit/*') || Request::is('admin/priests/view/*') ? 'active' : '') }}}">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Priests
                <i class="fas fa-angle-left right"></i>
                <!-- <span class="badge badge-info right">6</span> -->
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ URL('/admin/priests') }}" class="nav-link {{{ (Request::is('admin/priests') || Request::is('admin/priests/add') || Request::is('admin/priests/edit/*') || Request::is('admin/priests/view/*') ? 'active' : '') }}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>All priests</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ URL('/admin/priests/id') }}" class="nav-link {{{ (Request::is('admin/priests/id') ? 'active' : '') }}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Priests ID</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ URL('/admin/logout') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
              Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  @show
  @yield('content')
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>2020&nbsp;&copy;&nbsp;<a href="" target="_blank" class="kt-link">Pala Diocese</a></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- jQuery -->
<script src="{{ URL::asset('storage/demo/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ URL::asset('storage/demo/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ URL::asset('storage/demo/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ URL::asset('storage/demo/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ URL::asset('storage/demo/plugins/moment/moment.min.js') }}"></script>
<script src="{{ URL::asset('storage/demo/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ URL::asset('storage/demo/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ URL::asset('storage/demo/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ URL::asset('storage/demo/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('storage/demo/dist/js/adminlte.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<!-- <script src="{{ URL::asset('storage/demo/dist/js/demo.js') }}"></script> -->
<!-- DataTables -->
<script src="{{ URL::asset('storage/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('storage/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('storage/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ URL::asset('storage/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
    $(function () {
        $("#example1").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    });
</script>
@yield('js')
@stack('scripts')
</body>
</html>
