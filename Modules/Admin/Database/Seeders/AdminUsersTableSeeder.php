<?php

namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\Admin;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class AdminUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Model::unguard();
        if(DB::table('admin_users')->get()->count() == 0){
            $tasks  =   [
                        ['name' => 'John Doe',
            'email' => 'diocese@palai.in',
            'password' => bcrypt('diocese123'),
            'status' => 1
                        ]
                        ];
        $role = Role::create(['guard_name' => 'admin', 'name' => 'Administrator']);
        $role->givePermissionTo(Permission::all());
        $permissions = Permission::pluck('id','id')->all();
        $role->syncPermissions($permissions);

        DB::table('admin_users')->insert($tasks);
        $user=Admin::where('id', '1')->first();
        $user->assignRole([$role->id]);
        }
        Model::unguard();
    }
}
