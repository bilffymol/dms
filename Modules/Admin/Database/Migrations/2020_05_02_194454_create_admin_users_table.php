<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',255)->collation('utf8mb4_unicode_ci')->nullable(false);
            $table->string('designation',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('username',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('email',255)->collation('utf8mb4_unicode_ci')->nullable(false);
            $table->string('password',255)->collation('utf8mb4_unicode_ci')->nullable(false);
            $table->string('image',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->boolean('isAdmin')->nullable();
            $table->tinyInteger('prevstatus')->comment('0-inactive , 1-active')->nullable();
            $table->tinyInteger('status')->comment('0-inactive , 1-active')->nullable(false);
            $table->string('banner',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('logo',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('phone',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->text('address')->collation('utf8mb4_unicode_ci')->nullable();
            $table->text('location')->collation('utf8mb4_unicode_ci')->nullable();
            $table->text('latitude')->collation('utf8mb4_unicode_ci')->nullable();
            $table->text('longitude')->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('fb_url',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('insta_url',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('twitter_url',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('linkedin_url',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->text('aboutme')->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('remember_token',100)->nullable();
            $table->string('clicks',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('session_id',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->timestamp('reset_time')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_users');
    }
}
