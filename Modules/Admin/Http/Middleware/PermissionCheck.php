<?php

namespace Modules\Admin\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
class PermissionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next ,$permission = null,$guard="admin")
    {
         if (!Auth::guard($guard)->check()) 
        {
            if ($request->ajax()) 
            { 
                return response([
                    'error' => 'unauthorized',
                    'error_description' => 'Failed authentication.',
                    'data' => [],
                ], 401);
            } 
            else 
            {
                return redirect('/admin/login');
            }

        }
        else{
            $user = Auth::guard($guard)->user(); 
            $roles = $user->load('roles')->roles->toArray();
            
            if(!empty($roles))
            {
                if(!$user->hasRole('Administrator')){
                     // $role_perms = Role::with('permissions')->where('id',$roles[0]['id'])->get();
                    $permission_values = $user->getPermissionsViaRoles()->toArray();
                    foreach ($permission_values as $permission_val) {
                    $permissions[] = $permission_val['name'];
                    }
                    if(!in_array($permission,$permissions)){ return redirect('/admin/acces-denied');}
                }
            }else{
                return redirect('/admin/logout');
            }
          
        }
        return $next($request);
    }
}
