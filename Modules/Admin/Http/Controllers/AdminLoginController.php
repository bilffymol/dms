<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Modules\Admin\Entities\Admin;
use Carbon\Carbon;
use Auth;use Session;
use Cache;use URL;
class AdminLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() 
    {
        if(Auth::guard('admin')->user()) {
            return view('admin::pages.dashboard');
        }else{
            return view('admin::admin.login');
        }
    }

    /**
     * checking admin login details.
     * @param email
     * @param password
     * @return Response json array
     */

    public function post_login(Request $request)
    { 
        $request->validate([ 'email' => 'required|email', 'password' => 'required']);
        $credentials = $request->only('email', 'password');
        $credentials['status']  =   1;
        $remember_me = $request->has('remember_me') ? true : false;
        if (Auth::guard('admin')->attempt($credentials, $remember_me) && Auth::guard('admin')->user()->status == 1)
        {
            $user_id=Auth::guard('admin')->user()->id;
            if(Session::getId() != Auth::guard('admin')->user()->session_id){
              $admin_check = Admin::select('email','clicks','id','session_id','password')->find(Auth::guard('admin')->user()->id);
              Session::getHandler()->destroy($admin_check->session_id);
              $request->session()->regenerate();
              $admin_check->session_id = Session::getId();
              $admin_check->password = bcrypt($request->password);
              $admin_check->save();
            }else{
              $user_check->session_id = Session::getId();
              $user_check->save();
            }
            if(Admin::find($user_id)->hasAnyRole(Role::all())){
                return response()->json(['status'=>true,'csrf' => csrf_token(),'url'=>URL::to('/admin/dashboard/')]);
            }else{
            $html='<div class="alert alert-danger" role="alert"><strong>Alert!&nbsp;</strong>Invalid login or password.</div>';
            return response()->json(['status' => false,'alert'=>$html]);
            }
        }
        else
        {
            $html='<div class="alert alert-danger" role="alert"><strong>Alert!&nbsp;</strong>Invalid login or password.</div>';
            return response()->json(['status' => false,'alert'=>$html]);
        }
    }

  /**
     * Show the form for creating a new resource.
     * @return Response
     */

    /**
     * logout for admin users.
     * @return Response
     */
    public function logout()
    {
        Auth::guard('admin')->logout();
        Cache::flush();
        return redirect('/admin/login');
    }

    /**
     * Checking user Login.
     * @return true or false
     */
    public function CheckLogin()
    {
        if(Auth::guard('admin')->user())
        {
          return response()->json(['status'=>true,'csrf' => csrf_token()]);
        }
        else
        {
         return response()->json(['status'=>false,'csrf' => csrf_token(),'url'=>URL::to('/admin/')]);
        }
    }


     /**
     * Checking valid.
     * @return true or false
     */
    public function not_found()
    {
       return view('admin::admin.404');
    }

        public function admin_forgot_password(Request $request)
    {
        $request->validate(['email_reset' => 'required:admin_users,email,NULL,id']);
        $checkuser = Admin::select('id','name','email','reset_time','clicks')->where('email',$request->email_reset)->first();
        $request->session()->put('checkuser',$checkuser);
        if($checkuser)
        {
            return response()->json(['status'=>true,'csrf' => csrf_token(),'url'=>URL::to('/admin/reset_password/'.$checkuser->email)]);
        }
        else
        {
            $html='<div class="alert alert-danger" role="alert"><strong>Alert!&nbsp;</strong>Invalid email.</div>';
            return response()->json(['status' => false,'alert'=>$html]);
        }


    }

    public function admin_reset_password(Request $request,$email)
    {
        $resettime = Admin::where('email',$email)->select('reset_time','clicks')->first();
        $checkuser_send = Admin::select('id','name','email','reset_time','clicks')->where('email',$email)->first();
        $time_now = Carbon::now()->toDateTimeString();
        if($resettime->reset_time){
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $time_now);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $resettime->reset_time);
            $diff_in_days = $to->diffInHours($from);
            if($diff_in_days < 24 && $resettime->clicks == 0){
                return view('admin::admin.reset_password',compact('checkuser_send'));  
            }else{
                return view('admin::admin.404');
            }
        }else{return view('admin::admin.reset_password',compact('checkuser_send'));  }
    }

    public function admin_change_password( $id, Request $request)
    {
        $user = Admin::find($id);
        $user->password = bcrypt($request->CPassword);
        try
        {
            $user->clicks = 1;
            $user->save();
            $html='<div class="alert alert-success" role="alert"><strong>Success!&nbsp;</strong>You have reset your password.</div>';
            return response()->json(['status'=>true,'alert'=>$html,'url'=>URL('/admin/login/'),'csrf' => csrf_token()]);
        }catch (\Exception $e)
        {
            $html='<div class="alert alert-danger" role="alert"><strong>Alert!&nbsp;</strong>Password not saved.</div>';
            return response()->json(['status' => false,'alert'=>$html]);
        }
    }
}
