<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class Admin extends Authenticatable
{
	use Notifiable, HasRoles;
	protected $guard_name = 'admin';
    protected $table    = "admin_users";
    protected $fillable = ['name','email','password'];
    protected $hidden   = ['password','remember_token'];
    protected $dates    = ['deleted_at'];

}
