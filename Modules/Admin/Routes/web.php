<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['prevent-back-history','web'], 'prefix' => 'admin'], function()
{
    Route::get('/', 'AdminLoginController@index');
    Route::get('/login', 'AdminLoginController@index');
    Route::post('/post_login', 'AdminLoginController@post_login');
    Route::post('/forgot_password', 'AdminLoginController@admin_forgot_password');
    Route::get('/reset_password/{email}', 'AdminLoginController@admin_reset_password');
    Route::post('/change_password/{id}', 'AdminLoginController@admin_change_password');
    Route::get('/logout', 'AdminLoginController@logout');
    Route::get('/CheckLogin', 'AdminLoginController@CheckLogin');
    Route::get('/404', 'AdminLoginController@not_found');
    
    /* logged admin user opertaions */
	Route::group(['middleware' =>  'admin_auth:admin'], function(){
        Route::get('/', 'AdminLoginController@index');
   		Route::get('/dashboard', 'AdminLoginController@index');
	});
});
