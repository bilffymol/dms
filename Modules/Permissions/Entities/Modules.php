<?php

namespace Modules\Permissions\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Modules extends Model
{
    protected $table = "modules";
    protected $fillable = ['module_name','slug']; 
    protected $dates = ['deleted_at'];

    public function permissions(){
        return $this->hasMany('Modules\Permissions\Entities\Permissions', 'module_id','id');
    }
}
