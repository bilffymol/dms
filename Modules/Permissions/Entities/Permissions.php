<?php

namespace Modules\Permissions\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Permissions extends Model
{
    protected $table = "module_permissions";
    protected $fillable = ['name','slug','module_id','status']; 

    public function modules() {
        return $this->belongsTo("Modules\Permissions\Entities\Modules","module_id","id");
    }
}
