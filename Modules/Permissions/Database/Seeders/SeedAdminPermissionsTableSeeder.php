<?php

namespace Modules\Permissions\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Permissions\Entities\Permissions;

class SeedAdminPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

       $permissions = [

           ['id' =>'1','module_name'=>'List Roles','name'=>'role-list','guard_name' => 'admin','module_id'=>'1'],
           ['id' =>'2','module_name'=>'Create Roles','name'=>'role-create','guard_name' => 'admin','module_id'=>'1'],
           ['id' =>'3','module_name'=>'Edit Roles','name'=>'role-edit','guard_name' => 'admin','module_id'=>'1'],
           ['id' =>'4','module_name'=>'Delete Roles','name'=>'role-delete','guard_name' => 'admin','module_id'=>'1'],
           ['id' =>'5','module_name'=>'List Users','name'=>'user-list','guard_name' => 'admin','module_id'=>'2'],
           ['id' =>'6','module_name'=>'Create Users','name'=>'user-create','guard_name' => 'admin','module_id'=>'2'],
           ['id' =>'7','module_name'=>'Edit Users','name'=>'user-edit','guard_name' => 'admin','module_id'=>'2'],
           ['id' =>'8','module_name'=>'Delete Users','name'=>'user-delete','guard_name' => 'admin','module_id'=>'2'],
           ['id' =>'9','module_name'=>'List Priests','name'=>'priest-list','guard_name' => 'admin','module_id'=>'3'],
           ['id' =>'10','module_name'=>'Create Priests','name'=>'priest-create','guard_name' => 'admin','module_id'=>'3'],
           ['id' =>'11','module_name'=>'Edit Priests','name'=>'priest-edit','guard_name' => 'admin','module_id'=>'3'],
           ['id' =>'12','module_name'=>'Delete Priests','name'=>'priest-delete','guard_name' => 'admin','module_id'=>'3'],
           ['id' =>'13','module_name'=>'List Priests ID','name'=>'priest-id-list','guard_name' => 'admin','module_id'=>'3']
        ];


        foreach ($permissions as $permission) {

             Permissions::create($permission);

        }
    }
}
