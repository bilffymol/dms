<?php

namespace Modules\Permissions\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;


class PermissionsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [

           'role-list',
           'role-create',
           'role-edit',
           'role-delete',
           'user-list',
           'user-create',
           'user-edit',
           'user-delete',
           'priest-list',
           'priest-create',
           'priest-edit',
           'priest-delete',
           'priest-id-list'
        ];

        foreach ($permissions as $permission) {

             Permission::create(['guard_name' => 'admin','name' => $permission]);

        }

        /* 1 */
        if( $this->call(SeedAdminModulesListTableSeeder::class))
        $this->command->info('Table admin Module seeded!');
        /* 2 */
        if( $this->call(SeedAdminPermissionsTableSeeder::class))
        $this->command->info('Table admin permissions seeded!');
    }
}
