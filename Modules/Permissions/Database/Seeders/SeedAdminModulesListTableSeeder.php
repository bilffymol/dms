<?php

namespace Modules\Permissions\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Permissions\Entities\Modules;

class SeedAdminModulesListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $tasks = [
           ['id' =>'1','module_name'=>'Roles','slug'=>'roles','icon' => 'fa fa-cog','status'=>'1'],
           ['id' =>'2','module_name'=>'Users','slug'=>'users','icon' => 'fa fa-cog','status'=>'1'],
           ['id' =>'3','module_name'=>'Priests','slug'=>'priests','icon' => 'fa fa-cog','status'=>'1']
        ];

        
        foreach ($tasks as $task) {

             Modules::create($task);

        }
    }
}
