<?php

namespace Modules\Roles\Entities;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
	protected $guard_name = 'admin';
    protected $table = "roles";
    protected $fillable = ['name','guard_name'];
    protected $dates = ['deleted_at'];
}
